import numpy as np

from markov import choose_dict

from dandelion.check import perfect_rhyme

def match_strategy(strategy, words, prev_word, weight=None):
    matching_words = {w: p for w, p in words.items() if strategy(w, prev_word)}
    unmatching_words = {w: p for w, p in words.items() if not strategy(w, prev_word)}
    
    if len(matching_words) != 0:
        if len(unmatching_words) != 0 and weight is not None:
            probs = [weight / (1 + weight), 1 / (1 + weight)]
            words = np.random.choice([matching_words, unmatching_words], p=probs)
        else:
            words = matching_words
            
    return choose_dict(words)

def aliteration(doc, words, weight=None):
    prev_word = doc[-1]
    strategy = lambda w, prev_word: w != '\n' and w[0] == prev_word[0]
            
    return match_strategy(strategy, words, prev_word, weight)

def shorts(doc, words, weight=None):
    prev_word = doc[-1]
    strategy = lambda w, prev_word: w == '\n' and prev_word != '\n'
            
    return match_strategy(strategy, words, prev_word, weight)

def longs(doc, words, weight=None):
    prev_word = doc[-1]
    strategy = lambda w, prev_word: not (w == '\n' and prev_word != '\n')
            
    return match_strategy(strategy, words, prev_word, weight)

def rhyme(doc, words, weight=None):
    lines = ' '.join(doc).split('\n')
    if len(lines) < 2: return choose_dict(words)
    prev_word = lines[-2].split(' ')
    if len(prev_word) < 2: return choose_dict(words)
    prev_word = prev_word[-2]
    
    strategy = lambda w, prev_word: w != '\n' and perfect_rhyme(w, prev_word)
            
    return match_strategy(strategy, words, prev_word, weight)