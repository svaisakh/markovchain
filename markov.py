import numpy as np

class MarkovChain:
    def __init__(self, files, m=2, next_word=None, sample=1):
        from pathlib import Path

        self.m = m
        self.repo = {}
        self.seeds = set()
        self.next_word = next_word
        if next_word is None: self.next_word = lambda doc, words: choose_dict(words)

        if isinstance(files, Path) and files.is_dir(): files = list(files.glob('*'))
        if not isinstance(files, (tuple, list)): files = [files]

        for file in files:
            dataset = self.get_data(file, sample)
            self.train(dataset)

        self.seeds = list(self.seeds)

    def get_data(self, file, sample):
        with open(file) as f: data = f.readlines()

        data = data[:int(len(data) * sample)]
        data = ' '.join(d[:-1] + ' \n' if len(d) > 2 and d[-3] != ' ' else d for d in data)
        data = data.split(' ')
        return ([data[i + j] for j in range(self.m + 1)] for i in range(len(data) - self.m))

    def train(self, dataset):
        for data in dataset:
            if data[0] == '\n': self.seeds.add(' '.join(data[1:]))
            self.add(' '.join(data[:-1]), data[-1])

    def add(self, x, y, amount=1):
        try: self.repo[x]
        except KeyError: self.repo[x] = {}

        try: self.repo[x][y] += amount
        except KeyError: self.repo[x][y] = amount

    def __call__(self, l=300, seed=None):
        if seed is None: seed = np.random.choice(self.seeds)

        doc = seed.split(' ')
        for _ in range(l):
            last = ' '.join(doc[-self.m:])
            try: doc.append(self.next_word(doc, self.repo[last]))
            except KeyError: raise RuntimeError(f'Token {last} could not be found')

        print(' '.join(doc))

def choose_dict(d):
    probs = [v for v in d.values()]
    sum_probs = sum(probs)
    probs = [p / sum_probs for p in probs]
    keys = [k for k in d.keys()]
    return np.random.choice(keys, p=probs)

class MultiMarkovChain(MarkovChain):
    def __init__(self, files, m=5, sample=1):
        super().__init__(files, m, sample)

    def train(self, dataset):
        for data in dataset:
            if data[0] == '\n':
                for i in range(2, len(data) + 1): self.seeds.add(' '.join(data[1:i]))

            for i in range(1, len(data)):
                self.add(' '.join(data[:i]), data[i], amount=2**i)

    def __call__(self, l=300, seed=None, probabilistic=False):
        if seed is None: seed = np.random.choice(self.seeds)

        doc = seed.split(' ')
        for _ in range(l):
            words = {}

            for i in range(-self.m, 0):
                last = ' '.join(doc[i:])
                try:
                    for k, v in self.repo[last].items():
                        try: words[k] += v
                        except KeyError: words[k] = v

                    if not probabilistic: break

                except KeyError: pass

            if len(words) == 0: raise RuntimeError('A certain token cannot be found')

            doc.append(self.get_word(words))

        print(' '.join(doc))

def clean_data(filename):
    with open(filename, 'rb') as f: data = f.read()
    data = data.decode(errors='ignore')
    data = data.replace('\t', ' ')
    with open(filename, 'w') as f: f.write(data)